package biom4st3r.mod.foodstuff;

import java.util.function.Function;
import java.util.function.Supplier;

import biom4st3r.mod.foodstuff.block.FermentationBox;
import biom4st3r.mod.foodstuff.block.FermentationBoxItem;
import biom4st3r.mod.foodstuff.block.be.FermentationBoxBE;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public enum BlockEnum {
    FERMENTATION_BOX("fermentation_box", new FermentationBox(null), block->new FermentationBoxItem(block, new Item.Settings()), ()->new FermentationBoxBE()),
    ;

    public Block block;
    public BlockItem item;
    public Identifier id;
    public BlockEntityType<?> type;
    BlockEnum(String name, Block block) {
        this.id = new Identifier(ModInit.MODID, name);
        this.block = Registry.register(Registry.BLOCK, id, block);
    }
    BlockEnum(String name, Block block, Item.Settings settings) {
        this.id = new Identifier(ModInit.MODID, name);
        this.block = Registry.register(Registry.BLOCK, id, block);
        this.item = Registry.register(Registry.ITEM, id, new BlockItem(block, settings));
    }
    BlockEnum(String name, Block block, Function<Block,BlockItem> func) {
        this.id = new Identifier(ModInit.MODID, name);
        this.block = Registry.register(Registry.BLOCK, id, block);
        this.item = Registry.register(Registry.ITEM, id, func.apply(this.block));
    }
    BlockEnum(String name, Block block, Item.Settings settings, Supplier<BlockEntity> supplier) {
        this.id = new Identifier(ModInit.MODID, name);
        this.block = Registry.register(Registry.BLOCK, id, block);
        this.item = Registry.register(Registry.ITEM, id, new BlockItem(block, settings));
        this.type = Registry.register(Registry.BLOCK_ENTITY_TYPE, id, BlockEntityType.Builder.create(supplier, this.block).build(null));
    }
    BlockEnum(String name, Block block, Function<Block,BlockItem> func, Supplier<BlockEntity> supplier) {
        this.id = new Identifier(ModInit.MODID, name);
        this.block = Registry.register(Registry.BLOCK, id, block);
        this.item = Registry.register(Registry.ITEM, id, func.apply(this.block));
        this.type = Registry.register(Registry.BLOCK_ENTITY_TYPE, id, BlockEntityType.Builder.create(supplier, this.block).build(null));
    }
    
    @Environment(EnvType.CLIENT)
    void magicRegistry() {

        
    }
    public static void classload() {}
}
