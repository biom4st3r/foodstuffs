package biom4st3r.mod.foodstuff;

import net.fabricmc.api.ModInitializer;

public class ModInit implements ModInitializer {
	public static final String MODID = "foodstuff";

	@Override
	public void onInitialize() {
		BlockEnum.classload();
	}
}
