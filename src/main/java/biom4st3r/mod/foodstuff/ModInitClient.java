package biom4st3r.mod.foodstuff;

import java.util.Map;

import net.minecraft.block.BlockState;
import net.minecraft.client.color.world.BiomeColors;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.model.BakedModel;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;

import biom4st3r.mod.foodstuff.block.FermentationBox;

/**
 * ModInitClient
 */
public class ModInitClient implements ClientModInitializer {

    @Environment(EnvType.CLIENT)
    public static Map<BlockState, BakedModel> blockModels;

    @Override
    @Environment(EnvType.CLIENT)
    public void onInitializeClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(BlockEnum.FERMENTATION_BOX.block, RenderLayer.getCutout());
        ColorProviderRegistry.BLOCK.register((state,world,pos,tint)-> {
            if(state.get(FermentationBox.INOCULATED)) {
                return (BiomeColors.getWaterColor(world, pos) | 0x007F7F00) ^ 0x00000050;
            } else {
                return BiomeColors.getWaterColor(world, pos);
            }
        }, BlockEnum.FERMENTATION_BOX.block);
        // BlockModels models = MinecraftClient.getInstance().getBakedModelManager().getBlockModels();
        // blockModels = FieldRef.<Map<BlockState, BakedModel>>getFieldGetter(BlockModels.class, null, 0).getOrSet(models, null);
    }
    
}