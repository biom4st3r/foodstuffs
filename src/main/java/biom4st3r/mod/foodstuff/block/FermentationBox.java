package biom4st3r.mod.foodstuff.block;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import biom4st3r.mod.foodstuff.BlockEnum;
import biom4st3r.mod.foodstuff.block.be.FermentationBoxBE;
import biom4st3r.mod.foodstuff.yeast.Yeast;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.tool.attribute.v1.FabricToolTags;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class FermentationBox extends Block implements BlockEntityProvider {
    public static final BooleanProperty OPEN = BooleanProperty.of("open");
    public static final BooleanProperty FILLED = BooleanProperty.of("filled");
    public static final BooleanProperty INOCULATED = BooleanProperty.of("inoculated");
    static List<FermentationBox> children = Lists.newArrayList();

    public static void initStack(ItemStack stack) {
        if(!stack.getOrCreateTag().contains("states")) {
            CompoundTag tag = stack.getOrCreateTag();
            tag.putBoolean(OPEN.getName(), false);
            tag.putBoolean(FILLED.getName(), false);
            tag.putBoolean(INOCULATED.getName(), false);
        }
    }

    public FermentationBox(List<Block> parentTextures) {
        super(
            FabricBlockSettings
                .of(Material.WOOD)
                .breakByHand(true)
                .breakByTool(FabricToolTags.AXES)
                // .ticksRandomly()
                .nonOpaque()
        );
        this.setDefaultState(this.stateManager.getDefaultState()
            .with(OPEN, true)
            .with(FILLED, false)
            .with(INOCULATED, false)
            );
    }
    static VoxelShape CLOSED_MODEL = VoxelShapes.union(
        createCuboidShape(2, 0, 2, 14, 14, 14), 
        createCuboidShape(1, 14, 1, 15, 16, 15));
    static VoxelShape OPEN_MODEL = createCuboidShape(2, 0, 2, 14, 15, 14);
    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        if(state.get(OPEN)) {
            return OPEN_MODEL;
        }
        return CLOSED_MODEL;
    }

    @Override
    public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        super.randomTick(state, world, pos, random);
    }

    @Override
    protected void appendProperties(Builder<Block, BlockState> builder) {
        super.appendProperties(builder);
        builder.add(OPEN, FILLED, INOCULATED);
    }

    static CompoundTag toTag(BlockState state, CompoundTag tag) {
        tag.putBoolean(OPEN.getName(), state.get(OPEN));
        tag.putBoolean(FILLED.getName(), state.get(FILLED));
        tag.putBoolean(INOCULATED.getName(), state.get(INOCULATED));
        return tag;
    }

    @Override
    public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
        super.onBreak(world, pos, state, player);
        BlockEntity entity = world.getBlockEntity(pos);
        if(entity.getType() == BlockEnum.FERMENTATION_BOX.type) {
            FermentationBoxBE be = (FermentationBoxBE) entity;
            if(!world.isClient && player != null && !player.isCreative()) {
                ItemStack stack = new ItemStack(BlockEnum.FERMENTATION_BOX.item);
                if(be.getYeast() != null) stack.getOrCreateTag().put("yeast", be.getYeast().toTag(new CompoundTag()));
                stack.getOrCreateTag().put("state", toTag(state, new CompoundTag()));
                ItemEntity item = new ItemEntity(world, pos.getX()+0.5D, pos.getY(), pos.getZ()+0.5D, stack);
                item.setToDefaultPickupDelay();
                world.spawnEntity(item);
            }
        }
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack itemStack) {
        super.onPlaced(world, pos, state, placer, itemStack);
        BlockEntity entity = world.getBlockEntity(pos);
        if(entity.getType() == BlockEnum.FERMENTATION_BOX.type) {
            CompoundTag stateTag = itemStack.getOrCreateSubTag("state");
            if(!world.isClient && itemStack.getOrCreateTag().contains("state")) {
                BlockState newState = state
                    .with(OPEN, stateTag.getBoolean(OPEN.getName()))
                    .with(FILLED, stateTag.getBoolean(FILLED.getName()))
                    .with(INOCULATED, stateTag.getBoolean(INOCULATED.getName()));
                world.setBlockState(pos, newState);
                FermentationBoxBE be = (FermentationBoxBE) entity;
                be.setYeast(Yeast.fromTag(itemStack.getOrCreateTag().getCompound("yeast")));
            }
        }
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
            BlockHitResult hit) {
        if(state.get(FermentationBox.OPEN)) {
            world.setBlockState(pos, state.with(OPEN, false), 2);
        } else {
            world.setBlockState(pos, state.with(OPEN, true), 2);
        }
        return ActionResult.SUCCESS;
    }

    @Override
    public BlockEntity createBlockEntity(BlockView world) {
        return new FermentationBoxBE();
    }
    
}
