package biom4st3r.mod.foodstuff.block;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FluidBlock;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.decoration.ArmorStandEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.tag.FluidTags;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.world.RaycastContext.FluidHandling;
import net.minecraft.world.World;

public class FermentationBoxItem extends BlockItem {

    public FermentationBoxItem(Block block, Settings settings) {
        super(block, settings);
    }

    @Override
    public void onCraft(ItemStack stack, World world, PlayerEntity player) {
        CompoundTag tag = stack.getOrCreateTag();
        tag.putBoolean(FermentationBox.OPEN.getName(), false);
        tag.putBoolean(FermentationBox.FILLED.getName(), false);
        tag.putBoolean(FermentationBox.INOCULATED.getName(), false);
    }

    private static ItemStack onUse(ItemStack stack, World world, PlayerEntity user, Hand hand) {
        FermentationBox.initStack(user.getStackInHand(hand));
        HitResult r = raycast(world, user, FluidHandling.SOURCE_ONLY);
        if(r.getType() == HitResult.Type.BLOCK && !user.getStackInHand(hand).getOrCreateTag().getCompound("state").getBoolean(FermentationBox.FILLED.getName())) {
            BlockHitResult hit = (BlockHitResult) r;
            BlockState hitBlock = world.getBlockState(hit.getBlockPos());
            if(hitBlock.getFluidState().isIn(FluidTags.WATER)) {
                Fluid f = ((FluidBlock)hitBlock.getBlock()).tryDrainFluid(world, hit.getBlockPos(), hitBlock);
                if(f == Fluids.EMPTY) return ItemStack.EMPTY;
                
                ItemStack handStack = user.getStackInHand(hand); 
                ItemStack newStack = user.getStackInHand(hand).copy();
                handStack.decrement(1);
                newStack.getOrCreateTag().getCompound("state").putBoolean(FermentationBox.FILLED.getName(), true);
                newStack.setCount(1);

                return newStack;
            }
        }
        return ItemStack.EMPTY;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = onUse(user.getStackInHand(hand), world, user, hand);
        if(stack.isEmpty()) return super.use(world, user, hand);
        return TypedActionResult.success(user.getStackInHand(hand));
        
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        ItemStack stack = onUse(context.getPlayer().getStackInHand(context.getHand()), context.getWorld(), context.getPlayer(), context.getHand());
        if(stack.isEmpty()) return super.useOnBlock(context);
        return ActionResult.SUCCESS;
    }

    @Override
    public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
        CompoundTag tag = stack.getOrCreateTag().getCompound("state");
        if(tag.isEmpty()) return;
        if(!tag.getBoolean(FermentationBox.OPEN.getName())) tooltip.add(new LiteralText("Closed"));
        if(tag.getBoolean(FermentationBox.FILLED.getName())) tooltip.add(new LiteralText("Filled"));
        if(tag.getBoolean(FermentationBox.INOCULATED.getName())) tooltip.add(new LiteralText("Inoculated"));
    }
}
