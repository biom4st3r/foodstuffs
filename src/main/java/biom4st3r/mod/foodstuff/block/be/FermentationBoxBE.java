package biom4st3r.mod.foodstuff.block.be;

import biom4st3r.mod.foodstuff.BlockEnum;
import biom4st3r.mod.foodstuff.yeast.Yeast;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Tickable;

public class FermentationBoxBE extends BlockEntity implements Tickable {

    Yeast yeast;

    public FermentationBoxBE() {
        super(BlockEnum.FERMENTATION_BOX.type);
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        if(yeast != null) tag.put("yeast", yeast.toTag(new CompoundTag()));
        return super.toTag(tag);
    }
    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);
        if(tag.contains("yeast")) {
            yeast = Yeast.fromTag(tag.getCompound("yeast"));
        }
    }

    public Yeast getYeast() {
        return yeast;
    }

    public void setYeast(Yeast yeast) {
        this.yeast = yeast;
    }

    @Override
    public void tick() {
        // Biome biome = this.world.getBiome(this.pos);
        // Biome.Category cat = biome.getCategory();
    }
    
}
