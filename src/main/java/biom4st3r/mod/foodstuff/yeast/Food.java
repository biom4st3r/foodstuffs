package biom4st3r.mod.foodstuff.yeast;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Sets;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public abstract class Food {
    public static final Food SUGAR = newFood(Items.SUGAR, Items.SUGAR_CANE, Items.BEETROOT, Items.BEETROOT_SOUP, Items.SWEET_BERRIES, Items.APPLE, Items.PUMPKIN, Items.MELON_SLICE, Items.GLISTERING_MELON_SLICE);
    public static final Food VODKA = newFood(Items.POTATO, Items.CARROT, Items.POISONOUS_POTATO);
    public static final Food WHISKEY = newFood(Items.WHEAT);
    public static final Food KUMIS = newFood(Items.MILK_BUCKET);

    public abstract boolean isValidFood(Item i);
    public abstract CompoundTag toTag(CompoundTag tag);
    public static Food fromTag(CompoundTag tag) {
        switch(tag.getByte("type")) {
            case 1:
                return new SpecificFood(Registry.ITEM.get( new Identifier(tag.getString("item")) ));
            case 2: {
                ListTag litems = (ListTag) tag.get("items");
                Item[] items = litems.stream().map(Object::toString).map(Identifier::new).map(Registry.ITEM::get).toArray(Item[]::new);
                return new ListFood(items);
            }
            default:
                throw new RuntimeException("unknown food type");
        }
    }

    public static Food newFood(Item item) {
        return new SpecificFood(item);
    }
    public static Food newFood(Item... items) {
        return new ListFood(items);
    }

    public static class SpecificFood extends Food {

        private Item item;

        public SpecificFood(Item i) {
            this.item = i;
        }

        @Override
        public boolean isValidFood(Item i) {
            return item == i;
        }

        @Override
        public CompoundTag toTag(CompoundTag tag) {
            tag.putByte("type", (byte)1);
            tag.putString("item", Registry.ITEM.getId(this.item).toString());
            return tag;
        }

    }
    public static class ListFood extends Food {

        private Set<Item> items;

        public ListFood(Item... items) {
            this.items = Sets.newHashSet(items);
        }

        public ListFood(List<Item> item) {
            this.items = Sets.newHashSet(item);
        }

        @Override
        public boolean isValidFood(Item i) {
            return items.contains(i);
        }

        @Override
        public CompoundTag toTag(CompoundTag tag) {
            return null;
        }

    }
}