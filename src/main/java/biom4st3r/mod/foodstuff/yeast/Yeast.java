package biom4st3r.mod.foodstuff.yeast;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.recipe.Ingredient;

public final class Yeast {
    
    public enum Type {
        Alcohol,
        Vinegar,
        Botulism,
        ;
    }

    public final Yeast.Type type;
    /**
     * the point where the output yeast starts dying
     */
    public final float maxOutputTolerance;
    /**
     * range for {@link #maxOutputTolerance}
     */
    public final float variabilty;

    public final Food food;
    
    public Yeast(Type type, float maxOutputTolerance, float variabilty, Food food) {
        // Ingredient
        this.type = type;
        this.maxOutputTolerance = maxOutputTolerance;
        this.variabilty = variabilty;
        this.food = food;
    }
    public CompoundTag toTag(CompoundTag tag) {
        tag.putByte("type", (byte) type.ordinal());
        tag.putFloat("tol", maxOutputTolerance);
        tag.putFloat("var", variabilty);
        tag.put("food", this.food.toTag(new CompoundTag()));
        return tag;
    }
    public static Yeast fromTag(CompoundTag tag) {
        if(tag.isEmpty()) return null;
        return new Yeast(Type.values()[tag.getByte("type")], tag.getFloat("tol"), tag.getFloat("var"), Food.fromTag(tag.getCompound("food")));
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Yeast) {
            Yeast ob = (Yeast) obj;
            return ob.type == this.type && ob.maxOutputTolerance == this.maxOutputTolerance && ob.variabilty == this.variabilty;
        }
        return false;
    }
}
